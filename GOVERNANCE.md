Roles and Responsibilities

* Maintainer
* Contributor
* Committer

Decisionmaking Processes

We seek consensus in our decisionmaking and to build the standard from a collaborative and cooperative perspective. Meeting conveners will call for objections when it appears consensus has been reached, and anyone objecting to the apparent consensus should feel free to speak up. As needed, participants in the project can request a formal vote instead. We will review our governance processes regularly to keep them working effectively.

Recordkeeping

We are committed to transparency and an open process.
